#include "student.h"
#include "group.h"
#include "dekanat.h"

#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	
	Dekanat d;
	d.addNewGroupInFile();
	d.addNewStudentInFile();
	d.setAllRandHead();
	d.addRandMarks(10);
	cout << "========================================" << endl;
	cout << "��������� � ������� '2': " << d.howStudWithMinMark(2) << endl;
	cout << "========================================" << endl;
	d.transferStud(54, "381608-1");
	cout << "========================================" << endl;
	d.transferStud(38, "381608-1");
	cout << "========================================" << endl;
	cout << "������� ���� �� ���� �������: " << d.getAvMark() << endl;
	cout << "========================================" << endl;
	d.allBadStudDELETE();
	cout << "========================================" << endl;
	cout << "������� ���� �� ���� �������: " << d.getAvMark() << endl;
	cout << "========================================" << endl;
	d.getInfomation();
	d.saveBase();
}