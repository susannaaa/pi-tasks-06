#pragma once
#include <iostream>
#include "Student.h"
#define GR_TEXT_FILE "Groups.txt"
#define NEW_GR_TEXT_FILE "New_Groups.txt"
using namespace std;

class Student;
class Group
{
private:
	string gr_title;
	vector<Student*> stud; //������ ���������
	Student *head; //��������
public:
	Group(string title, Student* head) // ����������� ������ �� �������� ������ � ��������
	{
		this->gr_title = title;
		this->head = head;
		stud.reserve(50); //�������� ����� ��� ������ ���������
	}
	Group(string title) // ����������� ������ �� �������� ������ (�� ������ ������)
	{
		this->gr_title = title;
		stud.reserve(50); //�������� ����� ��� ������ ���������
	}

	void AddStud(Student* student) //��������� ��������
	{
		stud.push_back(student);
	}

	Student* FindStudent(int id) //����� �������� �� ����
	{
		for (unsigned int i = 0; i < stud.size(); i++)
		{
			if (stud[i]->GetId() == id)
				return stud[i];
		}
		return NULL;
	}

	string GetTitle() const //������ �������� ������
	{
		return gr_title;
	}

	void SetTitle(string title)//������ �������� ������
	{
		this->gr_title = title;
	}

	void SetHead(Student * head) //������ �������� ������
	{
		this->head = head;
	}

	Student* GetHead() const //������ ��������������
	{
		return head;
	}

	void DeleteStudent(Student* student) //������� ��������
	{
		for (unsigned int i = 0; i < stud.size(); i++)
		{
			if (student == stud[i])
			{
				stud[i] = stud.back();
				stud.resize(stud.size() - 1);
				break;
			}
		}
	}

	double AvgMark() //��������� ������� ������ ������
	{
		double avg = 0;
		for (unsigned int i = 0; i < stud.size(); i++)
			avg += stud[i]->GetAvgMark();
		avg /= stud.size();
		return avg;
	}

};