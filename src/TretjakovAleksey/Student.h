#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define STUD_TEXT_FILE "Students.txt"
#define NEW_STUD_TEXT_FILE "New_Students.txt"
#include <iostream>
#include <vector>
#include <time.h>
using namespace std;

class Group;
class Student
{
private:
	string fio;
	Group *group;
	int id;

public:
	vector<int> marks;
	Student(int id = 0, string fio = "", Group *gr = NULL) //����������� �������� �� ���, ���� � ������
	{
		this->fio = fio;
		this->id = id;
		this->group = gr;
		marks.reserve(10); // �������� ����� ��� ������ ������
	}

	int SizeOfMarks()	//������� ���������� ������(����� � ������� ������)
	{
		return marks.size();
	}


	int GetId() const //������ ����
	{
		return id;
	}

	void AddMark(int new_mark) // ���������� ������ � ������ ������
	{
		marks.push_back(new_mark);
	}

	double GetAvgMark() // ��������� ������� ������ ��������
	{
		double avg = 0;
		for (unsigned int i = 0; i < marks.size(); i++)
		{
			avg += marks[i];
		}
		avg /= marks.size();
		return avg;
	}

	void SetGroup(Group* group) //������ ������
	{
		this->group = group;
	}

	Group* GetGroup() const //������ ������
	{
		return group;
	}

	void AddRandomMarks() //��������� ��������� ������ 
	{
		srand(time(NULL));
		for (int i = 0; i < SizeOfMarks(); i++)
		{
			marks[i] = (int)(rand() % 5 + 1);
		}
	}

	string GetFIO() const //������ ���
	{
		return fio;
	}

	void SetFIO(string fio) //������ ���
	{
		this->fio = fio;
	}

	void SetId(int id) //������ ����
	{
		this->id = id;
	}


};