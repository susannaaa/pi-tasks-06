#include "Group.h"
#include<time.h>

Group::Group()
{
	name = 0;
	number = 0;
	st = nullptr;
	Head = nullptr;
}

Group::Group(int name)
{
	this->name = name;
}

void Group::setGroup(int name)
{
	this->name = name;
	number = 0;
}

void Group::addStudent(Student * s)
{
	if (number == 0) {
		st = new Student*[1];
		st[number] = s;
	}
	else
	{
		Student**tmp = new Student*[number + 1];
		for (int i = 0; i < number; i++)
			tmp[i] = st[i];
		delete[]st;
		st = tmp;
		st[number] = s;
	}
	s->setGroup(this);
	number++;
}

void Group::setHead()
{
	srand(time(NULL));
	int i = rand() % number;
	Head = st[i];
	cout << endl;
	cout << Head->fio << " староста  " << this->name << endl;
}

Student * Group::getHead()
{
	return Head;
}

int Group::getName()
{
	return name;
}

int Group::getNumder()
{
	return number;
}

Student* Group::fouStudent(int id)
{
	int found = -1;
	for (int i = 0; i < number; i++) {
		if (st[i]->id == id) {
			found = i;
			break;
		}
		if (found >= 0) return st[found];
		else return nullptr;
	}
}

Student * Group::fouStudent(string fio)
{
	int found = -1;
	for (int i = 0; i < number; i++) {
		if (st[i]->fio == fio) {
			found = i;
			break;
		}
		if (found >= 0) return st[found];
		else return nullptr;
	}
}

double Group::midMark()
{
	int s = 0;
	int count = 0;
	int i = 0;
	for (i = 0; i < number; i++) {
		for (int j = 0; j < st[i]->number; j++)
			s = s + st[i]->marks[j];
		count = count + st[i]->number;
	}
	return s / count;
}

void Group::delStudent(int id)
{
	int found = -1;
	int i = 0;
	for (i = 0; i < number; i++) {
		if (st[i]->id == id) {
			found = i;
			break;
		}
		if (found >= 0) {
			st[found] = st[number - 1];
			number--;
		};
	}

}

void Group::showStudent()
{
	for (int i = 0; i < number; i++) {
		st[i]->showFio();
		cout << "_" << st[i]->getId();
	}
	cout << endl;
}




Group::~Group()
{
	delete[]st;
}