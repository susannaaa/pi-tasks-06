#include "Dekanat.h"
#include "Group.h"
#include "Student.h"
#include <fstream>
#include <iostream>
#include<string>
#include <time.h>
using namespace std;
//�����������
Dekanat::Dekanat()
{
	numG = 0;
	numS = 0;
}
//����������
Dekanat::~Dekanat()
{
}

void Dekanat::LoadGroups()
{
	cout << "\n List of groups:\n" << endl;
	ifstream file("Group.txt");
	if (!file.is_open())
		cout << "file not found";
	//��������� � ������� ���������� � �������
	while (!file.eof())
	{
		int title;
		file >> title;
		if (numG == 0)
		{
			Groups = new Group*[numG + 1];
			Groups[numG] = new Group(title);
			cout <<" "<< Groups[numG]->gettitle()<< endl;
		}
		else
		{
			Group **tmp = new Group*[numG + 1];
			for (int i = 0; i < numG; i++)
				tmp[i] = Groups[i];
			delete[]Groups;
			Groups = tmp;
			Groups[numG] = new Group(title);
			cout <<" "<< Groups[numG]->gettitle() << endl;
		}
		numG += 1;
	}
	file.close();
}

void Dekanat::LoadStudents()
{
	cout << "\n List of students:\n" << endl;
	ifstream file("Students.txt");
	if (!file.is_open())
		cout << "Sorry, but file not found";
	//��������� � ������� ������ ���������
	while (!file.eof())
	{
		int id;
		string fio, f, n, o;
		int gr;
		file >> id >> f >> n >> o >> gr;
		fio = f + " " + n + " " + o;
		if (numS == 0)
		{
			Students = new Student*[numS + 1];
			Students[numS] = new Student(id, fio);
		}
		else
		{
			Student** tmp = new Student*[numS + 1];
			for (int i = 0; i < numS; i++)
				tmp[i] = Students[i];
			delete[]Students;
			Students = tmp;
			Students[numS] = new Student(id, fio);
		}
		for (int i = 0; i < numG; i++)
		{
			if (gr == Groups[i]->gettitle())
				Groups[i]->AddStud(Students[numS]);
		}
		cout <<" "<< Students[numS]->getID() << " " << Students[numS]->getFio() << endl;
		numS++;
	}
	file.close();
}

void Dekanat::Addmarks()
{
	cout << "\n Marks:\n" << endl;
	srand(time(NULL));
	//������������ ��������� ������
	for (int i = 0; i < numS; i++)
	{
		cout <<" "<< Students[i]->getID() << " " << Students[i]->getFio()<<":\n"<< " "<<"\n";
		Students[i]->AddMarks();
		cout << "\n";
	}
}

void Dekanat::Statistic()
{
	cout << "\n Top students and educational performance of groups:\n" << endl;
	for (int i = 0; i < numS; i++)
	if (Students[i]->AverMark() == 5)
		cout << " A-student - " << Students[i]->getFio() << endl;
	cout << "\n Average score in the groups:\n " << endl;
	for (int i = 0; i < numG; i++)
		cout <<" "<< Groups[i]->gettitle() << " - " << Groups[i]->AverMark() << endl;
}
//����������  ��������� �� ��������������
void Dekanat::Deduction()
{
	//������ ����������� ���������
	cout << "\n List of expelled students for academic failure:\n" << endl;
	for (int i = 0; i < numS; i++)
	{
		if (Students[i]->badMark())
		{
			int id = Students[i]->getID();
			Students[i]->getGr()->DeletStud(id);
			cout <<" "<< Students[i]->getFio() << endl;
			Students[i]->setDeduction();
			for (int j = i; j < numS - 1; j++)
				Students[j] = Students[j + 1];
			i--;
			numS--;
		}
	}
}

void Dekanat::ChoiceHead()
{
	cout << "\n Heads of groups:\n" << endl;
	for (int i = 0; i < numG; i++){
		cout << " ";
		Groups[i]->ChoiceHead();
	}
}

void Dekanat::TransferStud()
{
	cout << "\n ******************";
	cout << "\n Transfer of students" << endl;
	cout << " ******************\n" << endl;
	int id;
	bool flag = true;
	cout << " Please, enter ID of the student to transfer to another group:\n" << endl;
	while (flag)
	{
		cin >> id;
		for (int i = 0; i < numS; i++)
		{
			if ((Students[i]->getID() == id) && !(Students[i]->getDeduction()))
			{
				cout << " Please, enter the number of the group from the list to which you want to transfer the student\n:" << endl;
				int gr;
				for (int j = 0; j < numG; j++)
				{
					if (Groups[j] != Students[i]->getGr())
						cout << Groups[j]->gettitle() << endl;
				}
				cin >> gr;
				Students[i]->getGr()->DeletStud(Students[i]->getID());
				for (int j = 0; j < numG; j++)
				{
					if (gr == Groups[j]->gettitle())
					{
						Groups[j]->AddStud(Students[i]);
						cout << " Your student " << Students[i]->getFio() << " was transfer in the group: " << Groups[j]->gettitle() << endl;
					}
				}
				flag = false;
			}
		}
		if (flag)
			cout << " The student with that ID does not exist, maybe he was expelled. " << endl<< " Please, try again"<<endl;
			
	}
}

void Dekanat::Save()
{
	ofstream newFile("Changes.txt");
	for (int i = 0; i < numS; i++)
	{
		newFile << "�" << Students[i]->getID() << " "
			<< Students[i]->getFio() << " ";
		int *mark = Students[i]->getMarks();
		for (int j = 0; j < 10; j++)
		{
			newFile << mark[j];
		}
		newFile << "  average score - " << Students[i]->AverMark() << " "
			<< "group" << Students[i]->getGr()->gettitle() << endl << endl;
	}
	newFile.close();
}