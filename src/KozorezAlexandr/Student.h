#pragma once
#define Add(x) push_back(x)
#include "stdc++.h"
using namespace std;

class Group;

class Student
{
	private:
		int ID;
		string NAME;
		Group* GR;
		vector<int> MARKS;
	public:
		Student(int ID = 0, string NAME="", Group* GR = NULL) 
		{
			this->ID = ID;
			this->NAME = NAME;
			this->GR = GR;
		}
		~Student() { MARKS.clear(); ID = 0; NAME = ""; }
		void addMark(int VALUE) { MARKS.Add(VALUE); }
		double getAverage()
		{
			double avg = 0.0;
			int n = MARKS.size();
			for (int i = 0;i < n;i++)
				avg += MARKS[i];
			return (avg / n);
		}
		void to(Group* GR) { this->GR = GR; }
		Group* from() { return GR; }
		int getID() const { return ID; }
		string getNAME() const { return NAME; }
};