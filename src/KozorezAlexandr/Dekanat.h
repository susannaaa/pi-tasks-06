#pragma once
#include "Group.h"

class Dekanat
{
private:
	vector<Group*> GR_LIST;
	vector<Student*> ST_LIST;
public:
	Dekanat(string PATH1, string PATH2)
	{
		setlocale(LC_ALL, "Russian");
		loadGR(PATH1);
		loadST(PATH2);
		initializeWork();
	}
	~Dekanat()
	{
		for (int i = 0;i < GR_LIST.size();i++) 
			delete GR_LIST[i];
		for (int i = 0;i < ST_LIST.size();i++) 
			delete ST_LIST[i];
		GR_LIST.clear(), ST_LIST.clear();
	}
	void loadGR(string PATH1)
	{
		ifstream file_1(PATH1);
		if (file_1.is_open() == false)
		{
			cout << " error.log.0: ����� Dekanat::loadGR: ���������� ��������: ���� ����� �� ������\n";
			exit(EXIT_FAILURE);
		}
		string TITLE = "";
		while (file_1 >> TITLE)
			GR_LIST.Add(new Group(TITLE));
		file_1.close();
	}
	void loadST(string PATH2)
	{
		ifstream file_2(PATH2);
		int ID = 1, n = 0;
		string NAME = "", TITLE = "", id;
		if (file_2.is_open() == false)
		{
			cout << " error.log.0: ����� Dekanat::loadST: ���������� ��������: ���� ����� �� ������\n";
			exit(EXIT_FAILURE);
		}
		getline(file_2, id);
		while (!file_2.eof())
		{
			getline(file_2, id);
			getline(file_2, NAME);
			getline(file_2, TITLE);
			ID = atoi(id.c_str());
			n = GR_LIST.size();
			for(int i=0;i<n;i++)
				if (TITLE == GR_LIST[i]->getTitle())
				{
					ST_LIST.Add(new Student(ID, NAME));
					ST_LIST.back()->to(GR_LIST[i]);
					GR_LIST[i]->add(ST_LIST.back());
				}
		}
		file_2.close();
	}
	Student* find(int ID)
	{
		int n = ST_LIST.size();
		for (int i = 0;i < n;i++)
			if (ST_LIST[i]->getID() == ID) return ST_LIST[i];
		return NULL;
	}
	Group* find(string TITLE)
	{
		int n = GR_LIST.size();
		for (int i = 0;i < n;i++)
			if (GR_LIST[i]->getTitle() == TITLE) return GR_LIST[i];
		return NULL;
	}
	void remove(Student *ST)
	{
		Group* GR = ST->from();
		if (GR->getHead() == ST) GR->setHead(NULL);
		ST->to(NULL);
		GR->remove(ST);
	}
	void remove(int ID)
	{
		Student* ST = find(ID);
		if (ST == NULL)
		{
			cout << " error.log.1: ����� Dekanat::remove: ���������� ��������: �������� �������� � ID = " << ID << "  �� �������\n";
			return;
		}
		remove(ST);
		int n = ST_LIST.size(), was = 0;
		for (int i = 0;i < n;i++)
			if (ST_LIST[i] == ST)
				ST_LIST[i] = ST_LIST.back(),
				ST_LIST.resize(n - 1),
				was = 1;
			else if (was == 1) break;
			cout << " success.log.1: ������� c ID = " << ID << " ������� ������ �� �������\n";
	}
	void add(Student* ST, Group *GR)
	{
		GR->add(ST);
		ST->to(GR);
	}
	void add(Student* ST, string TITLE)
	{
		int n = ST_LIST.size();

		Group* GR = find(TITLE);
		if (GR == NULL)
		{
			cout << " error.log.2: ������ � �" << TITLE << " �� �������\n";
			return;
		}
		for(int i=0;i<n;i++)
			if (ST_LIST[i]->getID() == ST->getID())
			{
				cout << " error.log.3: ID = " << ST->getID() << " �����\n";
				return;
			}
		ST->to(GR), ST_LIST.Add(ST), GR->add(ST);
		cout << " success.log.2: ������� � ID = " << ST->getID() << " ������� �������� � �������\n";
	}
	void transfer(int ID, string TITLE)
	{
		Student* ST = find(ID);
		Group* GR = find(TITLE);
		if (ST == NULL)
		{
			cout << " error.log.4: ������� � ID = " << ID << " �� ������\n";
			return;
		}
		if (GR == NULL)
		{
			cout << " error.log.5: ������ ��� �" << TITLE << " �� ������\n";
			return;
		}
		remove(ST), add(ST, GR);
		cout << " success.log.3: ������� ��� ID = " << ID << " ��������� � ������ �" << TITLE << "\n";
	}
	void addMark(int ID, int MARK)
	{
		find(ID)->addMark(MARK);
	}
	void addHead(string TITLE, int ID)
	{
		Group* GR = find(TITLE);
		Student* ST = find(ID);
		if (ST == NULL)
		{
			cout << " error.log.5: ������� � ID = " << ID << " �� ������\n";
			return;
		}
		if (GR == NULL)
		{
			cout << " error.log.6: ������ ��� �" << TITLE << " �� �������\n";
			return;
		}
		if (ST->from() != GR)
		{
			cout << " error.log.7: � ������ ��� �" << TITLE << " ��� �������� � ID = " << ID << "\n";
			return;
		}
		GR->setHead(ST);
		cout << " success.log.4: ������� � ID = " << ID << " ������ �������� ������ �" << TITLE << "\n";
	}
	void addMarks(int COUNT)
	{
		int n = ST_LIST.size();
		for (int i = 0;i < n;i++)
			for (int j = 0;j < COUNT;j++)
				addMark(ST_LIST[i]->getID(), (rand() % 5 + 1));
		cout << " success.log.5: ��������� ������ ���� ���������\n";
	}
	Group* bestGR()
	{
		int n = GR_LIST.size();
		if (n != 0) {
			Group* best = GR_LIST[0];
			for (int i = 0;i < n; i++)
				if (best->getAverage() < GR_LIST[i]->getAverage())
					best = GR_LIST[i];
			cout << " success.log.6: ������ ������� ����: "
				<< best->getAverage() << " ����� ������ �"
				<< best->getTitle() << "\n";
			return best;
		}
		else
		{
			cout << " error.log.8: ������� ��� ������� ���������� ������ ������ \n";
			return NULL;
		}
	}
	Student* bestST()
	{
		int n = ST_LIST.size();
		if (n != 0)
		{
			Student* best = ST_LIST[0];
			for (int i = 0;i < n;i++)
				if (best->getAverage() < ST_LIST[i]->getAverage())
					best = ST_LIST[i];
			cout << " success.log.7: ������ ������� ���� ����� ������� � ID = " << best->getID() << ": " <<
				best->getNAME() << " : " << best->getAverage() << "\n";
		}
		else
		{
			cout << " error.log.9: ������� ��� ������� ���������� ������� �������� \n";
			return NULL;
		}
	}
	void printlnST(string PATH3)
	{
		setlocale(LC_ALL, "Russian");
		ofstream file_3(PATH3);
		if (file_3.is_open() == false)
		{
			cout << " error.log.10: ���� ��� ������ ��������� �� ������ \n";
			exit(EXIT_FAILURE);
		}
		int n = ST_LIST.size();
		for (int i = 0;i < n;i++)
			file_3 << "\n ID: " << ST_LIST[i]->getID() << " \n Name: " << ST_LIST[i]->getNAME() 
			<< "\n Group: " << ST_LIST[i]->from()->getTitle() << "\n Average mark: " 
			<< ST_LIST[i]->getAverage() << "\n";
		file_3.close();
	}
	void initializeWork() 
	{

		add(new Student(3, "Kozorez Alexandr Alexeevich"), "381608-1");
		add(new Student(51, "Shushunov Nikolay Petrovich"), "381608-3");
		add(new Student(21, "Kozorez Alexandr Alexeevich"), "381608-1");
		transfer(21, "1-MK-GK");
		transfer(1, "381608-3");
		transfer(100, "381608-2");
		remove(1);
		remove(91);
		addHead("381608-1", 90);
		addHead("1-MK-GK", 21);
		addHead("381608-1", 1);
		addMarks(15);

		Group* GR = bestGR();
		Student* ST = bestST();
	}


};