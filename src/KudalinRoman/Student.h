#pragma once
#include <iostream>
#include <string>
using namespace std;

class Group;
class Student
{
private:
	int id;
	string fio;
	Group *gr;
	int *marks;
	int marksNumber;
public:
	Student() : id(0), fio(""), marks(nullptr),
		gr(nullptr), marksNumber(0) {};

	Student(int id, string fio)
	{
		this->id = id;
		this->fio = fio;
		marks = nullptr;
		gr = nullptr;
		marksNumber = 0;
	}

	Student(const Student &st)
	{
		id = st.id;
		fio = st.fio;
		gr = st.gr;
		marksNumber = st.marksNumber;
		if (marksNumber)
		{
			marks = new int[marksNumber];
			for (int i = 0; i < marksNumber; i++)
				marks[i] = st.marks[i];
		}
	}

	~Student() 
	{ 
		delete[] marks; 
		marks = nullptr;
	}

	void setID(int id) { this->id = id; }
	void setFIO(string fio) { this->fio = fio; }
	void setGroup(Group *gr) { this->gr = gr; }

	int getID() const { return id; }
	string getFIO() const { return fio; }
	Group* getGroup() const { return gr; }

	// ���������� ������ ��������.
	void addMark(int mark)
	{
		if (!marksNumber)
		{
			marks = new int[1];
			marks[marksNumber++] = mark;
		}
		else
		{
			int *temp = new int[marksNumber + 1];
			for (int i = 0; i < marksNumber; i++)
				temp[i] = marks[i];
			temp[marksNumber++] = mark;
			delete[] marks;
			marks = temp;
		}
	}

	// ������� ������ ��������.
	double getAvgMark()
	{
		double avgMark = 0.;
		if (!marksNumber)
			return 0;
		else
		{
			for (int i = 0; i < marksNumber; i++)
				avgMark += marks[i];
			avgMark /= marksNumber;
			return avgMark;
		}
	}

	// ����� ���������� �� ������� ��������.
	void showMarks(ostream& stream)
	{
		if (!marksNumber)
		{
			stream << "This student has got no marks" << endl;
			return;
		}
		else
		{
			for (int i = 0; i < marksNumber; i++)
				stream << marks[i] << " ";
			stream << endl;
		}
	}
};
