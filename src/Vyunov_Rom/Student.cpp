#include "Student.h"

void Student::setGroup(Group * gr)
{
	this->gr = gr;
}

void Student::addMark(double Mark)
{
	double * tmp=new double[Num+1];
	for (int i = 0; i < Num; i++)
	{
		tmp[i] = Marks[i];
	}
	delete[] Marks;
	Marks = tmp;
	Marks[Num] = Mark;
	Num++;
}

double Student::getAverageRating()
{
	double AverageRating = 0;
	double MarksSum = 0;
	for (int i = 0; i < Num; i++)
	{
		MarksSum += Marks[i];
	}
	AverageRating = MarksSum / Num;
	return AverageRating;
}

string Student::getFIO()
{
	return FIO;
}

int Student::getID()
{
	return ID;
}

Group * Student::getGroup()
{
	return gr;
}