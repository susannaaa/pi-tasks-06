#include "Dekanat.h"
Dekanat::Dekanat() {}

void Dekanat::LoadGroups() {
	ifstream fcin("Groups.txt");
	string tit;
	while (fcin >> tit)
		groups.push_back(new Group(tit));
}

void Dekanat::LoadStud() {
	ifstream fscin("Students.txt");
	int id;
	string fio, s1, s2, s3, tit;
	while (fscin >> id) {
		fscin >> s1 >> s2 >> s3;
		fio = s1 + " " + s2 + " " + s3;
		fscin >> tit;
		for (int i = 0; i < groups.size(); ++i) {
			if (tit == groups[i]->GetTitle()) {
				students.push_back(new Student(id, fio, groups[i]));
				groups[i]->AddStud(students.back());
			}
		}
	}
}

Student* Dekanat::FindStud(int id) {
	for (int i = 0; i < students.size(); ++i) {
		if (students[i]->GetId() == id)
			return students[i];
	}
	return NULL;
}

Group * Dekanat::FindGroup(string title)
{
	for (int i = 0; i < groups.size(); ++i) {
		if (groups[i]->GetTitle() == title)
			return groups[i];
	}
	return NULL;
}

void Dekanat::RemoveStudGroup(Student * stud) {
	Group* gr = stud->GetGroup();
	if (gr->GetHead() == stud)
		gr->SetHead(NULL);
	stud->SetGroup(NULL);
	gr->RemoveStudent(stud);
}

void Dekanat::AddStud(Student* stud, Group* gr) {
	gr->AddStud(stud);
	stud->SetGroup(gr);
}

void Dekanat::TransferStud(int id, string title) {
	Student* stud = FindStud(id);
	Group* gr = FindGroup(title);
	if (stud == NULL) {
		cout << "Student No" << id << " not found" << endl;;
		return;
	}
	if (gr == NULL) {
		cout << "Group No" << title << " not found" << endl;
		return;
	}
	RemoveStudGroup(stud);
	AddStud(stud, gr);
	cout << "Student No" << id << " successfuly transfered in group No" << title << endl;
}

void Dekanat::AddStud(Student* new_stud, string title) {
	for (int i = 0; i < students.size(); i++) {
		if (students[i]->GetId() == new_stud->GetId()) {
			cout << "Id �" << new_stud->GetId() << " is bussy" << endl;
			return;
		}
	}
	Group* gr = FindGroup(title);
	if (gr == NULL) {
		cout << "Group No" << title << " not found" << endl;
		return;
	}
	new_stud->SetGroup(gr);
	students.push_back(new_stud);
	gr->AddStud(new_stud);
	cout << "Student No" << new_stud->GetId() << " successfuly added in group No" << title << endl;
}

void Dekanat::RemoveStud(int id) {
	Student* stud = FindStud(id);
	if (stud == NULL) {
		cout << "Student No" << id << " not found" << endl;;
		return;
	}
	RemoveStudGroup(stud);
	for (int i = 0; i < students.size(); ++i) {
		if (students[i] == stud) {
			students[i] = students.back();
			students.resize(students.size() - 1);
			break;
		}
	}
	cout << "Student No" << id << " successfuly remove" << endl;
}

void Dekanat::AddMark(int id, int *mark) {
	FindStud(id)->AddMark(mark);
}

void Dekanat::AddHead(string title, int id) {
	Group* gr = FindGroup(title);
	Student* stud = FindStud(id);
	if (stud == NULL) {
		cout << "Student No" << id << " not found" << endl;;
		return;
	}
	if (gr == NULL) {
		cout << "Group No" << title << " not found" << endl;
		return;
	}
	if (stud->GetGroup() != gr) {
		cout << "Group No" << title << " haven't student No" << id << endl;
		return;
	}
	gr->SetHead(stud);
	cout << "Head (student No" << id << ") successfuly added in group No" << title << endl;
}


Dekanat::~Dekanat() {
	for (int i = 0; i < groups.size(); ++i)
		delete[] groups[i];
	groups.clear();
	for (int i = 0; i < students.size(); ++i)
		delete[] students[i];
	students.clear();
}
Group::Group(string title, Student* head) {
	this->title = title;
	this->head = head;
}

void Group::SetTitle(string title) {
	this->title = title;
}

void Group::SetHead(Student * head) {
	this->head = head;
}

string Group::GetTitle(){
	return title;
}

Student * Group::GetHead() {
	return head;
}

void Group::AddStud(Student * new_student) {
	staff.push_back(new_student);
}

Student * Group::FindStudent(int id) {
	for (int i = 0; i < staff.size(); ++i) {
		if (staff[i]->GetId() == id)
			return staff[i];
	}
	return NULL;
}

void Group::RemoveStudent(Student* stud) {
	for (int i = 0; i < staff.size(); ++i) {
		if (stud == staff[i]) {
			staff[i] = staff.back();
			staff.resize(staff.size() - 1);
			break;
		}
	}
}


Group::~Group() {
	for (int i = 0; i < staff.size(); ++i)
		delete[] staff[i];
	staff.clear();
}
Student::Student(int id, string fio, Group* group) {
	this->id = id;
	this->fio = fio;
	this->group = group;
}

void Student::SetId(int id) {
	this->id = id;
}

void Student::SetFio(string fio) {
	this->fio = fio;
}

void Student::SetGroup(Group* group) {
	this->group = group;
}

int Student::GetId() {
	return id;
}

string Student::GetFio() {
	return fio;
}

Group* Student::GetGroup() {
	return group;
}

void Student::AddMark(int *new_mark) {
	marks.push_back(new_mark);
}


Student::~Student() {
	for (int i = 0; i < marks.size(); ++i)
		delete[] marks[i];
	marks.clear();
}
