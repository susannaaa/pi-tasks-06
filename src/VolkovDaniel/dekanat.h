#define _CRT_SECURE_NO_WARNINGS
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;
class Group;
class Dekanat {
	vector<Group*> groups;
	vector<Student*> students;
public:
	Dekanat();
	void LoadGroups();
	void LoadStud();
	Student* FindStud(int id);
	Group* FindGroup(string title);
	void RemoveStudGroup(Student* stud);
	void TransferStud(int id, string title);
	void AddStud(Student* stud, Group* gr);
	void AddStud(Student* new_stud, string title);
	void RemoveStud(int id);
	void AddMark(int id, int *mark);
	void AddHead(string title, int id);
	~Dekanat();
};
class Student;
class Group {
private:
	string title;
	Student* head;
	vector<Student*> staff;
public:
	Group(string title = "", Student* head = NULL);
	void SetTitle(string title);
	void SetHead(Student* head);
	string GetTitle();
	Student* GetHead();
	void AddStud(Student*new_student);
	Student* FindStudent(int id);
	void RemoveStudent(Student* stud);
	~Group();
};
class Student {
private:
	int  id;
	string fio;
	Group* group;
	vector<int*> marks;
public:
	Student(int id = 0, string fio = "", Group* group = NULL);
	void SetId(int id);
	void SetFio(string fio);
	void SetGroup(Group* group);
	int GetId();
	string GetFio();
	Group* GetGroup();
	void AddMark(int *new_mark);
	~Student();
};