#include "student.h"


Student::Student(): fio(""),group(NULL),id(-1),grants(0){}
Student::Student(const Student &s):id(s.id)
{
	marks = s.marks;
    fio = s.fio;
    grants = s.grants;
    group = s.group;
}

Student::Student(int id, string s):group(NULL),grants(0)
{
    this->id = id;
    fio = s;
}
Student::~Student()
{

}

void Student::addMark(int mark)
{
	marks.push_back(mark);
}
double Student::getAvMark()
{
    double res=0;
    for(int i = 0; i<marks.size(); i++)
    {
        res+= marks[i];
    }
    res/= marks.size();
    return res;
}

int Student::getId()
{
    return id;
}

Group * Student::getGroup()
{
	return group;
}

string Student::getFio()
{
    return fio;
}

void Student::exitGroup()
{
	if (group == nullptr)
		return;
	group->removeStud(group->findStudent(*this));
	group = nullptr;
}

void Student::print()
{
    cout<<id<<" "<< fio<<"."<<endl;
}
void Student::setGroup(Group *gr)
{
    group = gr;
}
