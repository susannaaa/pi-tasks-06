#ifndef STUDENT
#define STUDENT

#include "group.h"
#include <vector>

using namespace std;

class Group;
class Student
{
public:
    Student();
    Student(const Student &s);
    Student(int id, std::string s);
    void addMark(int mark);
    void setGroup(Group *gr );
    double getAvMark();
    int getId();
	Group* getGroup();
    std::string getFio();
	void exitGroup();
    void print();

    ~Student();
private:
    int id;
    vector<int> marks;
    std::string fio;
    int grants;
    Group *group;
};
#endif // STUDENT

