#include <iostream>
#include "group.h"
#include "dekanat.h"
#include "student.h"
#include <fstream>
#include <time.h>

using namespace std;
//сценарий. Создание деканата, файлы.

void printAll(Dekanat &deka)
{
	deka.print();
	deka.printHeads();
	deka.printStatisticGroups();
	deka.printStatisticStudents();
	deka.printTheBest();
}

int main()
{
	srand(time(0));
    setlocale(LC_ALL,"Russian");
    Dekanat dekan;
    dekan.loadStudents("students.txt");
    dekan.loadGroups("groups.txt");
    dekan.distributeToGroups();
	dekan.setHeads();
    dekan.print();
	dekan.setMarks();
	dekan.removeBedStudents();
    dekan.print();
	dekan.saveGroups();
	dekan.saveStud();

	dekan.moveStud(7, "381607-3");
	dekan.moveStud(13, "381607-3");
	dekan.moveStud(23, "381608-1");
	printAll(dekan);


    return 0;
}

