﻿#include "Group.h"
#include<time.h>
Group:: Group()
{
	Title = 0;
	size = 0;
	Staff = nullptr;
	head = nullptr;
}
Group::Group(int name)
{
	Title = name;
	size = 0;
}
int Group::getTitle()
{
	return Title;
}
void Group::addStudent(Student *S)
{
	if (size == 0) {
		Staff = new Student*[size + 1];
		Staff[size] = S;
	}
	else {
		Student **copy = new Student*[size + 1];
		for (int i = 0; i < size; i++)
		{
			copy[i] = Staff[i];
		}
		delete[] Staff;
		Staff = copy;
		Staff[size] = S;
	}
	S->setGroup(this);
	size++;
}
Student*  Group:: findStudent(int id)
{
	int i; bool flag = false;
	for (i = 0; i < size; i++)
		if (Staff[i]->ID == id)
		{
			flag = true;
			break;
		}
	if (flag = true) return(Staff[i]);
	else return(nullptr);
}
void Group::RemoveStudent(int id)
{
	int i;
	for (i = 0; i < size; i++)
	{
		if (Staff[i]->getId() == id) break;
	}
	int temp = i;
	Staff[temp] = Staff[size];
	size--;
}
void Group::setHead()
{
	srand(time(NULL));
	int i = rand() % size;
	head = Staff[i];
	cout << head->fio << " староста группы " << this->Title << endl;
}
Student* Group::getHead()
{
	return(head);
}
float Group::getAvMark()
{
	int i;
	float ave = 0, sum = 0;
	for (i = 0; i < size; i++)
		sum += Staff[i]->getAvMark();
	ave = sum / size;
	return ave;
}
